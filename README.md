# Privacy Services

A service providing REST endpoints to access privacy algorithms, privacy and utility metrics and POI detection attacks on geodata.

Install this package via pip:
```bash
pip install git+https://git@git.informatik.uni-leipzig.de/scads/de4l/privacy/de4l-privacy-services.git
```
## Development
Clone the repository and change into the directory
```
git clone https://git.informatik.uni-leipzig.de/scads/de4l/privacy/de4l-privacy-services.git

cd de4l-privacy-services 
```
The project is managed with [poetry](https://python-poetry.org/). Follow the instructions on [https://python-poetry.org/docs/](https://python-poetry.org/docs/) to install poetry.
Install the dependencies with:
```
poetry install
poetry shell
pip install torch
deactivate
```
Now you can run the development server with:
```
poetry run python de4l_privacy_services/services/host_privacy_services.py
```
To continue development of the [*Privacy Tuna*](https://github.com/majaschneider/privacytuna) application, also check the 
[frontend](https://git.informatik.uni-leipzig.de/scads/de4l/privacy/de4l-private-routes-frontend) and 
the second [backend](https://git.informatik.uni-leipzig.de/scads/de4l/privacy/de4l-private-routes-backend).

## Docker
The [*Privacy Tuna*](https://github.com/majaschneider/privacytuna) application requires *Privacy Services* to
run in a docker container. Build the container with:
```bash
docker build -t privacy-services:latest .
```
