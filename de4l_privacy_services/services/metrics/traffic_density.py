"""Utility metric to calculate the traffic density.
"""
import math

import grid_density_utility_metric.route_to_grid as r2g
from de4l_geodata.geodata.point import Point, get_distance
from de4l_geodata.geodata.route import Route
import numpy as np


def calculate_ratio_of_point_count(input_routes, ground_truth_routes, grid_origin=None, grid_width=None,
                                   width_per_cell=250):
    """
    Create a grid and count the number of points in each cell based on the routes' and the ground_truth_routes' points.
    For each cell calculate as utility value the ratio of the input routes' point count and the ground truth routes'
    point count. Each point of the input routes is assigned the utility value of the cell that it lies in. Utility
    values lie between 0 and 100, where 0 denotes no utility and 100 denotes maximum utility. If the input routes and
    the ground truth routes are the same, the utility value is 100 for each point. If a cell contains more points from
    input routes than from ground truth routes, the utility of the respective cell is limited to the maximum value of
    100.

    Parameters
    ----------
    input_routes : :obj:`list` of :obj:`Route`
        The list of input routes to calculate the metric for.
    ground_truth_routes : :obj:`list` of :obj:`Route`
        The list of routes to compare the input routes with. Serves as ground truth.
    grid_origin: Point
        The origin of the grid (being the lower left grid point). Needs to be in the same geo reference system as the
        routes' points, e.g. 'latlon'.
    grid_width: float
        The extension of the grid in meters.
    width_per_cell: float
        The width of a cell in meters. Cells are defined as squares. The default is 1 kilometer.

    Returns
    -------
    utility_values : dict
        dataset : int
            The relative utility of all points averaged over the whole data set.
        route : :obj:`list` of :obj:`int`
            The relative utility of all points in a route averaged per route.
        point : :obj:`list` :obj:`list` of :obj:`int`
            The relative utility for each point in each route.
    """
    if len(input_routes) == 0:
        raise 'Routes are empty.'
    if len(ground_truth_routes) != len(input_routes):
        raise 'Number of input routes and ground truth routes needs to be the same.'
    coordinates_unit = None
    for routes in [input_routes, ground_truth_routes]:
        for route in routes:
            if len(route) > 0:
                if route.get_geo_reference_system() != 'latlon':
                    raise "Geo reference system needs to be latlon."
                if coordinates_unit is None:
                    coordinates_unit = route.get_coordinates_unit()
                if route.get_coordinates_unit() != coordinates_unit:
                    raise "Coordinates unit is not the same for all input and ground truth routes."
    if input_routes == ground_truth_routes:
        utility_per_point = {'dataset': 100, 'route': [100 for _ in input_routes],
                             'point': [[100 for _ in route] for route in input_routes]}
        return utility_per_point

    coordinates_unit_is_radians = coordinates_unit == 'radians'
    for routes in [input_routes, ground_truth_routes]:
        for route in routes:
            route.to_radians_(ignore_warnings=True)

    if grid_origin is None or grid_width is None:
        area = get_coverage_area(input_routes + ground_truth_routes)
        grid_origin = area['origin']
        grid_lower_right_point = grid_origin.add_vector(area['extension'], math.radians(90))
        grid_width = get_distance(grid_origin, grid_lower_right_point)
    else:
        grid_lower_right_point = grid_origin.add_vector(grid_width, math.radians(90))
    grid_width_x_lon = grid_lower_right_point.x_lon - grid_origin.x_lon
    nr_grid_cells_per_row = max(1, int(grid_width / width_per_cell))

    grid_calculator = r2g.RouteToGrid(grid_origin, grid_width_x_lon, nr_grid_cells_per_row)

    grid_cnt_input_routes = grid_calculator.routes_to_grid_count(input_routes)
    grid_cnt_ground_truth_routes = grid_calculator.routes_to_grid_count(ground_truth_routes)
    grid_cnt_ratio = grid_cnt_input_routes / grid_cnt_ground_truth_routes
    for i, row in enumerate(grid_cnt_ratio):
        for j, col in enumerate(row):
            if np.isnan(col) or np.isinf(col):
                grid_cnt_ratio[i][j] = 0
            if grid_cnt_ratio[i][j] > 1:
                grid_cnt_ratio[i][j] = 1

    utility_per_point = []
    utility_per_route = []
    utility_per_dataset = 0
    for route in input_routes:
        utility_points_in_route = []
        if len(route) > 0:
            for point in route:
                # find the utility value of the corresponding grid cell of this point
                try:
                    x, y = grid_calculator.route_to_grid_idx(
                        Route([point], coordinates_unit=point.get_coordinates_unit())
                    )[0]
                    utility_points_in_route.append(grid_cnt_ratio[x, y].item() * 100)
                except:
                    utility_points_in_route.append(0)
                    # debugging the case that a point is lying outside the defined grid
                    print('A point is lying outside of the defined grid.')
                    print(len(route))
                    print(point)
                    print(grid_calculator.route_to_grid_idx(Route([point], coordinates_unit=point.get_coordinates_unit())))
                    print(grid_calculator.origin, grid_calculator.grid_width)
                    print(ground_truth_routes + input_routes)
        else:
            utility_points_in_route.append(0)
        utility_route = np.average(utility_points_in_route)
        utility_per_dataset += utility_route
        utility_per_route.append(utility_route)
        utility_per_point.append(utility_points_in_route)
    utility_per_dataset /= len(input_routes)

    if not coordinates_unit_is_radians:
        for routes in [input_routes, ground_truth_routes]:
            for route in routes:
                route.to_degrees_()

    utility_values = {'dataset': utility_per_dataset, 'route': utility_per_route, 'point': utility_per_point}
    return utility_values


def calculate_ratio_of_desired_point_count(input_routes, ground_truth_routes, grid_origin=None, grid_width=None,
                                           width_per_cell=1_000, desired_point_count_per_cell=10):
    """
    Evaluate the utility of input routes with regard to a) their correctness and b) their utility regarding a
    desired point count. Create a grid based on the specifications for origin, width and number of cells per row, where
    each cell is supposed to have the given point count. As a default a cell of 1 square kilometers should contain
    at least 10 points.

    1) Check the correctness of each comparable route by comparing the point count per grid cell to the route's
    counterpart in the ground truth routes. Adjust the point count per cell of each input route to not be greater than
    in the corresponding ground truth route. This is achieved by eliminating points from the input route that don't
    belong into the cell they are in.

    2) Check the data's utility with respect to a desired point count per cell. For each cell calculate the ratio
    of the count of actual (but corrected) and desired points. If the actual point count is greater or equal than the
    desired, the maximum utility is reached.

    Return utility values for each point, route and the whole route data set as follows:

    a) per data set: For each cell calculate the ratio of actual (corrected) points from the input routes to the
    desired value per cell and average all cells (also those, that have zero points).
    b) per route: For each cell that was touched by either the input or its corresponding ground truth route,
    calculate the ratio of actual (corrected) points from the input routes to the desired value per cell and
    average the cell values.
    c) per point: Assign to a point the route's utility of that cell per route, where the point is located in.
    (Alternative: Assign to a point the average utility of its route.)

    Utility values lie between 0 and 100, where 0 denotes no utility and 100 denotes maximum utility.

    Parameters
    ----------
    input_routes : :obj:`list` of :obj:`Route`
        The list of input routes to calculate the metric for.
    ground_truth_routes : :obj:`list` of :obj:`Route`
        The list of routes to compare the input routes with. Serves as ground truth.
    grid_origin: Point
        The origin of the grid (being the lower left grid point). Needs to be in the same geo reference system as the
        routes' points, e.g. 'latlon'.
    grid_width: float
        The extension of the grid in meters.
    width_per_cell: float
        The width of a cell in meters. Cells are defined as squares. The default is 1 kilometer.
    desired_point_count_per_cell : int
        The minimum number of points that need to be present in a cell to achieve the maximum utility of 100. Needs to
        be at least 1.

    Returns
    -------
    utility_values : dict
        dataset : int
            Utility for the data set.
        route : :obj:`list` of :obj:`int`
            Utility for each route.
        point : :obj:`list` :obj:`list` of :obj:`int`
            Utility for each point in each route.
    """
    if len(input_routes) == 0:
        raise 'Routes are empty.'
    if len(ground_truth_routes) != len(input_routes):
        raise 'Number of input routes and ground truth routes needs to be the same.'
    coordinates_unit = None
    for routes in [input_routes, ground_truth_routes]:
        for input_route in routes:
            if len(input_route) > 0:
                if input_route.get_geo_reference_system() != 'latlon':
                    raise "Geo reference system needs to be latlon."
                if coordinates_unit is None:
                    coordinates_unit = input_route.get_coordinates_unit()
                if input_route.get_coordinates_unit() != coordinates_unit:
                    raise "Coordinates unit is not the same for all ground truth and input routes."
    if desired_point_count_per_cell < 1:
        desired_point_count_per_cell = 1
    if width_per_cell <= 0:
        width_per_cell = 1

    coordinates_unit_is_radians = coordinates_unit == 'radians'
    for routes in [input_routes, ground_truth_routes]:
        for route in routes:
            route.to_radians_(ignore_warnings=True)

    if grid_origin is None or grid_width is None:
        area = get_coverage_area(input_routes + ground_truth_routes)
        grid_origin = area['origin']
        grid_lower_right_point = grid_origin.add_vector(area['extension'], math.radians(90))
        grid_width = get_distance(grid_origin, grid_lower_right_point)
    else:
        grid_lower_right_point = grid_origin.add_vector(grid_width, math.radians(90))
    grid_width_x_lon = grid_lower_right_point.x_lon - grid_origin.x_lon
    nr_grid_cells_per_row = max(1, int(grid_width / width_per_cell))

    grid_calculator = r2g.RouteToGrid(grid_origin, grid_width_x_lon, nr_grid_cells_per_row)

    utility_per_route = []
    utility_per_point = []
    grid_cnt_desired = (grid_calculator.zero_grid() == 0).int() * desired_point_count_per_cell

    grid_utility_dataset = grid_calculator.zero_grid()
    for i in range(len(input_routes)):
        input_route = input_routes[i]
        ground_truth_route = ground_truth_routes[i]

        grid_cnt_input_route = grid_calculator.route_to_grid_count(input_route)
        grid_cnt_ground_truth_route = grid_calculator.route_to_grid_count(ground_truth_route)

        # eliminate points from the input route that are in a cell where there are no points from the ground truth route
        incorrect = (grid_cnt_ground_truth_route == 0).int() * (grid_cnt_input_route > 0).int()
        correct = (incorrect == 0).int()
        grid_cnt_corrected_input_route = \
            correct * grid_cnt_input_route + incorrect * grid_cnt_ground_truth_route

        # cut number of points off at the desired point count
        count_higher_than_desired = (grid_cnt_corrected_input_route > desired_point_count_per_cell).int()
        count_lower_equal_than_desired = (count_higher_than_desired == 0).int()
        grid_cnt_corrected_input_route = \
            count_lower_equal_than_desired * grid_cnt_corrected_input_route + \
            count_higher_than_desired * desired_point_count_per_cell

        grid_utility_route = (grid_cnt_corrected_input_route / grid_cnt_desired) * 100
        grid_utility_dataset += grid_utility_route

        # calculate average route utility based on those cells touched by either the ground truth or input route
        cells_to_consider = r2g.unify_grid(grid_calculator.routes_to_grid_count([input_route, ground_truth_route]))
        nr_cells = cells_to_consider.sum().item()
        sum_values = grid_utility_route.sum().item()
        utility_per_route.append(sum_values / nr_cells)

        # calculate utility per point based on route's utility per cell
        utility_points_in_route = []
        for point in input_route:
            # find the utility value of the corresponding grid cell of this point
            wrapped_point = Route([point], coordinates_unit=point.get_coordinates_unit())
            row, col = grid_calculator.route_to_grid_idx(wrapped_point)[0]
            utility_points_in_route.append(grid_utility_route[row, col].item())
        utility_per_point.append(utility_points_in_route)
    utility_per_dataset = float(np.average(grid_utility_dataset))

    if not coordinates_unit_is_radians:
        for routes in [input_routes, ground_truth_routes]:
            for route in routes:
                route.to_degrees_()

    utility_values = {'dataset': utility_per_dataset, 'route': utility_per_route, 'point': utility_per_point}
    return utility_values


def get_coverage_area(routes):
    """
    Return origin (lower left point) and extension of the minimum quadratic area covering all points of all routes.

    Parameters
    ----------
    routes : :obj:`list` of :obj:`Route`
        A list of routes.

    Returns
    -------
    minimum_coverage_area : dict
        origin : Point
            The lower left point of the area.
        extension : float
            The width of the area in meters.
    """
    all_x_coordinates = [point.x_lon for route in routes for point in route]
    all_y_coordinates = [point.y_lat for route in routes for point in route]
    min_x = np.min(all_x_coordinates)
    max_x = np.max(all_x_coordinates)
    min_y = np.min(all_y_coordinates)
    max_y = np.max(all_y_coordinates)
    unit = routes[0].get_coordinates_unit()

    max_distance = np.max([
        get_distance(Point([max_x, 0], coordinates_unit=unit), Point([min_x, 0], coordinates_unit=unit)),
        get_distance(Point([0, max_y], coordinates_unit=unit), Point([0, min_y], coordinates_unit=unit))]) + 0.1
    origin = Point([min_x, min_y], coordinates_unit=unit)
    minimum_coverage_area = {'origin': origin, 'extension': max_distance}
    return minimum_coverage_area
