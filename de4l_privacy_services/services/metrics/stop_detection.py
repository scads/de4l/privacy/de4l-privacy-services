"""
Stop detection algorithm to find points of interest (POI) in a route as described in
Primault, V. (2018), Practically Preserving and Evaluating Location Privacy.
"""
import numpy as np
from de4l_stop_detection.stop_detection import extract_pois
import pandas as pd
from de4l_geodata.geodata.route import get_distance


def calculate_stop_detection_metric(routes, true_pois, time_threshold=pd.Timedelta(0.2, 'min'),
                                    distance_threshold=300):
    """
    Calculate the points of interest (POI) of a route and compare with the true POIs. The privacy value refers to
    the accuracy, being the ratio of correctly detected POIs to all POIs.

    Parameters
    ----------
    routes: :obj:`list` of :obj:`Route`
        The input routes to calculate the metric for.
    true_pois: :obj:`list` of :obj:`Route`
        The true stop points (POIs) of the input routes.
    time_threshold : Timedelta
        The minimum time duration that has to be spent in every stay.
    distance_threshold : int
        The maximal diameter of the stay area in meters.

    Returns
    -------
    privacy_values : dict
        Privacy values for the dataset, the routes and the points. Values are normalized to be between 0 and 100, where
        0 indicates no privacy and 100 perfect privacy.
        dataset : int
            Privacy values for the data set.
        route : :obj:`list` of :obj:`int`
            Privacy values for each route.
        point : :obj:`list` :obj:`list` of :obj:`int`
            Privacy values for each point in each route.
    performance_measures : dict
        Performance values (precision, recall, f-score) of the metric for the dataset and the routes.
        dataset : dict
            Performance values for the dataset.
            precision : float
            recall : float
            f-score : float
        route : :obj:`list` of :obj:`dict`
            Performance values for the routes.
            precision : float
            recall : float
            f-score : float
    """
    if len(routes) == 0:
        raise 'Routes are empty.'
    coordinates_unit = None
    for route in routes:
        if len(route) > 0:
            if route.get_geo_reference_system() != 'latlon':
                raise "Geo reference system needs to be latlon."
            if coordinates_unit is None:
                coordinates_unit = route.get_coordinates_unit()
            if route.get_coordinates_unit() != coordinates_unit:
                raise "Coordinates unit is not the same for all input and ground truth routes."
    coordinates_unit_is_radians = coordinates_unit == 'radians'
    for route in routes:
        route.to_radians_(ignore_warnings=True)

    privacy_values_points = []
    privacy_values_routes = []
    performance_measures_routes = []
    for i, route in enumerate(routes):
        privacy_values_route = []
        detected_pois = extract_pois(route, time_threshold, distance_threshold)
        true_pois_route = true_pois[i]

        # for each true poi get its closest detected poi, that is closer than 200 m
        closest_detected_pois = []
        min_distances = []
        detected_true_pois = []
        for i_poi, true_poi in enumerate(true_pois_route):
            min_dist = 200
            min_dist_idx = None
            for detected_poi_idx, detected_poi in enumerate(detected_pois):
                dist = get_distance(detected_poi, true_poi)
                if dist < min_dist:
                    min_dist = dist
                    min_dist_idx = detected_poi_idx
                    detected_true_pois.append(i_poi)
            closest_detected_poi = detected_pois[min_dist_idx] if min_dist_idx is not None else None
            closest_detected_pois.append(closest_detected_poi)
            min_distances.append(min_dist)

        true_positives = len(set(detected_true_pois))
        precision = true_positives/len(detected_pois) if len(detected_pois) > 0 else 0.0
        recall = true_positives/len(true_pois_route) if len(true_pois_route) > 0 else 0.0
        performance_measures_routes.append({
            'precision': precision,
            'recall': recall,
            'f_score': 2*((precision*recall)/(precision+recall)) if precision+recall > 0 else 0.0
        })

        # filter those true pois where a detected poi is closer than 200 m
        if len(detected_pois) > 0:
            true_pois_route = [true_poi for true_poi in true_pois_route if
                               np.min([get_distance(true_poi, detected_poi) for detected_poi in detected_pois]) < 200]

        for point in route:
            privacy_value = 100
            if len(true_pois_route) > 0:
                # for each point find the closest true poi
                distances_to_true_pois = [get_distance(point, true_poi) for true_poi in true_pois_route]
                distance_closest_true_poi = np.min(distances_to_true_pois)
                idx_closest_true_poi = np.argmin(distances_to_true_pois)

                # if the closest true poi is closer than 200 m, check if the distance to the according detected poi is
                # also less than 200 m. If yes, assign the privacy value of the point proportional to the distance.
                if distance_closest_true_poi < 200:     # true poi is closer than 200 m to point
                    closest_detected_poi = closest_detected_pois[idx_closest_true_poi]
                    if closest_detected_poi is not None:    # according detected poi is closer than 500 m to point
                        dist = get_distance(point, closest_detected_poi)
                        if dist < 500:
                            privacy_value = int((dist / 500.) * 100)
            privacy_values_route.append(privacy_value)

        privacy_values_points.append(privacy_values_route)

        # for the route privacy value check only the actual true pois and the distance to their closest detected poi
        privacy_value_route = np.average([int((dist / 200.) * 100) for dist in min_distances]
                                         if len(min_distances) > 0 else [100])
        privacy_values_routes.append(privacy_value_route)

    # dataset privacy value is an average of the routes' values
    privacy_values_dataset = np.average(privacy_values_routes)
    privacy_values = {'dataset': privacy_values_dataset, 'route': privacy_values_routes, 'point': privacy_values_points}
    performance_measures = {
        'dataset': {
            'precision': sum(p['precision'] for p in performance_measures_routes) / len(performance_measures_routes),
            'recall': sum(p['recall'] for p in performance_measures_routes) / len(performance_measures_routes),
            'f_score': sum(p['f_score'] for p in performance_measures_routes) / len(performance_measures_routes)
        },
        'route': performance_measures_routes}

    if not coordinates_unit_is_radians:
        for route in routes:
            route.to_degrees_()

    return privacy_values, performance_measures
