"""Utility metric to calculate the euclidean distance between original and comparative routes.
"""

import numpy as np
from de4l_geodata.geodata.point import get_distance


def calculate_euclidean_distance(original_routes, comparative_routes, distance_limit=100):
    """
    Calculate utility by evaluating for each point of a comparative route the euclidean distance to the closest point in
    its corresponding original route. The smaller the distance, the greater the utility. Utility values lie between
    0 and 100, where 0 denotes no utility and 100 denotes maximum utility. If the original routes and the comparable
    routes are the same, the utility value is 100 for each point.

    Parameters
    ----------
    original_routes : :obj:`list` of :obj:`Route`
        The list of input routes to calculate the metric for.
    comparative_routes : :obj:`list` of :obj:`Route`
        The list of routes to compare the input routes with.
    distance_limit : int
        The distance limit in meters between a point and the closest point in its corresponding original route as of
        which utility is 0. If distances are smaller than distance_limit, the utility is not zero. The maximum
        utility is achieved when the distance is zero.

    Returns
    -------
    utility_values : dict
        dataset : int
            The relative utility of all points averaged over the whole data set.
        route : :obj:`list` of :obj:`int`
            The relative utility of all points in a route averaged per route.
        point : :obj:`list` :obj:`list` of :obj:`int`
            The relative utility for each point in each route.
    """
    if len(original_routes) == 0:
        raise 'Routes are empty.'
    if len(comparative_routes) != len(original_routes):
        raise 'Number of comparative routes and routes needs to be the same.'
    if original_routes == comparative_routes:
        utility_values = {'dataset': 100, 'route': [100 for _ in original_routes],
                          'point': [[100 for _ in route] for route in original_routes]}
        return utility_values

    utility_values_points = []
    for route_idx, route in enumerate(comparative_routes):
        utility_in_route = []
        if len(original_routes[route_idx]) > 0:
            for point in route:
                distance = np.min([get_distance(point, original_point) for original_point in original_routes[route_idx]])
                distance = np.min([distance, distance_limit])
                utility_point = ((distance_limit - distance) / distance_limit) * 100
                utility_in_route.append(utility_point)
        else:
            utility_in_route.append(0)
        utility_values_points.append(utility_in_route)

    # route and dataset utility values are averages of the points' values
    utility_values_routes = [np.average(points) for points in utility_values_points]
    utility_values_dataset = np.average(utility_values_routes)

    utility_values = {'dataset': utility_values_dataset, 'route': utility_values_routes, 'point': utility_values_points}
    return utility_values
