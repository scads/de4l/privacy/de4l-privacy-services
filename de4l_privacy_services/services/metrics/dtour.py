"""
D-Tour metric to find points of interest (POI) in a route as described in
Schneider et al. (2022), D-TOUR : Detour-based point of interest detection in privacy-sensitive trajectories.
"""

from geopy.geocoders import Nominatim
from de4l_detour_detection import detour_detection as dtour
from de4l_geodata.geodata.route import get_distance
import numpy as np
import requests


def get_true_positives(true_stops, detected_stops, max_distance):
    """
    Calculates the number of true-positives, i.e. points that have correctly been identified as a stop. A point from
    detected_stops is correctly identified as a true stop, when it is less than max_distance apart from any true stop.

    To every true stop, a detected stop is assigned, if the distance is smaller than max_distance

    Parameters
    ----------
    true_stops : Route
        The list of true stops/pois.
    detected_stops : Route
        The list of stops/pois that have been detected by an algorithm.
    max_distance : float
        The maximum distance in meters that a point is allowed to be apart from a true stop to be counted as such.

    Returns
    -------
    true-positives : int
        The number of true-positives.
    """
    detected_true_stops = []
    for i_true_stop, true_stop in enumerate(true_stops):
        for detected_poi in detected_stops:
            distance = get_distance(detected_poi, true_stop)
            if distance < max_distance:
                detected_true_stops.append(i_true_stop)

    return len(set(detected_true_stops))


def calculate_dtour_metric(routes, true_pois_per_route, service_name):
    """
    Calculate the points of interest (POI) of a route and compare with the true POIs. The privacy value refers to
    the accuracy, being the ratio of correctly detected POIs to all POIs.

    Parameters
    ----------
    routes: :obj:`list` of :obj:`Route`
        The list of input routes to calculate the metric for.
    true_pois_per_route: :obj:`list` of :obj:`Route`
        The true stop points (POIs) of the input routes in degrees.
    service_name: {'germany', 'portugal', 'california'} # todo: check available services
        The name of the routing and Nominatim service to use.

    Returns
    -------
    privacy_values : dict
        Privacy values for the dataset, the routes and the points. Values are normalized to be between 0 and 100, where
        0 indicates no privacy and 100 perfect privacy.
        dataset : int
            Privacy values for the data set.
        route : :obj:`list` of :obj:`int`
            Privacy values for each route.
        point : :obj:`list` :obj:`list` of :obj:`int`
            Privacy values for each point in each route.
    performance_measures : dict
        Performance values (precision, recall, f-score) of the metric for the dataset and the routes.
        dataset : dict
            Performance values for the dataset.
            precision : float
            recall : float
            f-score : float
        route : :obj:`list` of :obj:`dict`
            Performance values for the routes.
            precision : float
            recall : float
            f-score : float
    """
    ors_path = 'service.scadsai.uni-leipzig.de/de4l-privacy/open-route-' + service_name
    scheme = 'https'
    ors_profile = 'driving-car'
    nominatim_url = 'service.scadsai.uni-leipzig.de/de4l-privacy/nominatim-' + service_name
    nominatim = Nominatim(scheme=scheme, domain=nominatim_url)

    routes_risks = []
    routes_pois = []
    risks_flat = []
    privacy_values_points = []

    if true_pois_per_route is None:
        return [[100 for _ in route] for route in routes]

    # test if ors service is available
    try:
        get = requests.get('https://' + ors_path)
        if get.status_code != 200:
            print(f'ORS service for {service_name} is not available.')
            ors_path = None
    except requests.exceptions.RequestException as e:
        print(f'ORS service for {service_name} is not available.')
        ors_path = None

    try:
        for route in routes:
            if len(route) > 0:
                _, _, _, _, risks, pois, _ = \
                    dtour.calculate_poi_risk(route=route, spatial_distance=620, ors_path=ors_path, ors_scheme=scheme,
                                             ors_profile=ors_profile, nominatim=nominatim, sampling_distance=25,
                                             acceptable_distance_to_shortest_route=20, exceeding_for_maximum_risk=50,
                                             map_matching=False)

            else:
                risks = [[0.0]]
                pois = []
            routes_risks.append(risks)
            routes_pois.append(pois)
            for risk in risks:
                risks_flat.append(risk)
    except ValueError:
        print('Nominatim/ORS not available.')
        privacy_values_points = [[0. for _ in route] for route in routes]

    # privacy calculation per point:
    # for each point in the route check its risk of POI detection by D-Tour algorithm
    # if D-Tour detects a POI with some likelihood (risk not zero), check if the detected POI is less than 200 m
    # apart from an actual POI
    # if this is the case, the point is assigned the inverse POI detection risk, otherwise it has 100% privacy
    performance_measures_routes = []
    for route_idx in range(len(routes)):
        route = routes[route_idx]
        route_risks = routes_risks[route_idx]
        privacy_values_route = []
        for point_idx in range(len(route)):
            point = route[point_idx]
            point_risk = route_risks[point_idx]
            if point_risk > 0:
                min_dist_to_poi = 201     # initialize with a value greater than 200
                for true_poi in true_pois_per_route[route_idx]:
                    dist_to_poi = get_distance(true_poi, point)
                    if dist_to_poi < min_dist_to_poi:
                        min_dist_to_poi = dist_to_poi
                if min_dist_to_poi < 200:
                    privacy_values_route.append(100 - 100 * point_risk)
                else:
                    privacy_values_route.append(100)
            else:
                privacy_values_route.append(100)
        privacy_values_points.append(privacy_values_route)
        true_positives = get_true_positives(true_pois_per_route[route_idx], routes_pois[route_idx], 200)
        precision = true_positives/len(routes_pois[route_idx]) if len(routes_pois[route_idx]) > 0 else 0.0
        recall = true_positives/len(true_pois_per_route[route_idx]) if len(true_pois_per_route[route_idx]) > 0 else 0.0
        performance_measures_routes.append({
            'precision': precision,
            'recall': recall,
            'f_score': 2*((precision*recall)/(precision+recall)) if precision+recall > 0 else 0.0
        })

    # privacy calculation for the whole route is an average over the highest detection risk of each POI:
    # register for each actual POI the highest risk of those detected POIs that are less than 200 m apart
    privacy_values_routes = []
    for route_idx in range(len(routes)):
        route = routes[route_idx]
        route_risks = routes_risks[route_idx]
        true_pois = true_pois_per_route[route_idx]
        max_detection_risks_per_poi = []
        for true_poi_idx, true_poi in enumerate(true_pois):
            max_poi_detection_risk = 0
            for point_idx in range(len(route)):
                point = route[point_idx]
                point_risk = route_risks[point_idx]
                if point_risk > 0 and get_distance(true_poi, point) < 200 and point_risk > max_poi_detection_risk:
                    max_poi_detection_risk = point_risk
            max_detection_risks_per_poi.append(max_poi_detection_risk)
        privacy_value_route = 100 - 100 * np.average(max_detection_risks_per_poi) \
            if len(max_detection_risks_per_poi) > 0 else 100
        privacy_values_routes.append(privacy_value_route)

    # dataset privacy value is an average of the routes' values
    privacy_values_dataset = np.average(privacy_values_routes)
    privacy_values = {'dataset': privacy_values_dataset, 'route': privacy_values_routes, 'point': privacy_values_points}
    performance_measures = {
       'dataset': {
           'precision': sum(p['precision'] for p in performance_measures_routes) / len(performance_measures_routes),
           'recall': sum(p['recall'] for p in performance_measures_routes) / len(performance_measures_routes),
           'f_score': sum(p['f_score'] for p in performance_measures_routes) / len(performance_measures_routes)
       },
       'route': performance_measures_routes}

    return privacy_values, performance_measures
