"""Provides an enum for all available privacy algorithms.
"""

from aenum import AutoNumberEnum
from de4l_geodata.geodata.route import Route
from de4l_two_dimensional_noise.privacy_algorithm import noise2d, noise2d_global
from de4l_promesse.privacy_algorithm import promesse


class PrivacyAlgorithm(AutoNumberEnum):
    """Enum of all available privacy algorithms.
    """
    _init_ = 'route_name id display_name parameters description'
    NOISE2D_PER_POINT = \
        'noise2d_point', \
        0, \
        'Noise 2D Point', \
        [{'name': 'Epsilon',
          'unit': 'Ɛ',
          'logScale': True,
          'min': 0.1,
          'max': 0.005,
          'description': 'The privacy budget used to calculate the noise parameters.'}], \
        'Adds point-wise noise (drawn from a Laplace distribution) onto a trajectory to achieve ' \
        '(epsilon, delta)-Differential Privacy for each point. ' \
        'See Andrés, M. et al. (2013), Figure 3 on page 7.'
    NOISE2D_PER_ROUTE = \
        'noise2d_route', \
        1, \
        'Noise 2D Route', \
        [{'name': 'Epsilon',
          'unit': 'Ɛ',
          'logScale': False,
          'min': 100,
          'max': 10,
          'description': 'The privacy budget used to calculate the noise parameters.'}], \
        'Adds noise onto a trajectory to achieve (epsilon, delta)-Differential Privacy. The correlation between the ' \
        'points in the trajectory is taken into account and noise is added once on the complete trajectory. ' \
        'See Jiang, K. et al. (2013), Algorithm 2 on page 5.'
    PROMESSE = \
        'promesse', \
        2, \
        'Promesse', \
        [{'name': 'Alpha',
          'unit': 'm',
          'logScale': False,
          'min': 30.0,
          'max': 1000.0,
          'description': 'Minimum distance in meters between successive points. A larger Alpha provides better privacy '
                         'but leads to a higher loss of information.'
          }], \
        'Protects places of interest (POI) in a route of geographical points by sampling points along the shape of ' \
        'the original route, where successive points have a predefined distance of alpha from each other. ' \
        'This avoids point clusters, which indicate longer stays of a person and can therefore identify POIs.'

    @classmethod
    def get_available_endpoints(cls):
        """
        Returns the endpoints of the available privacy algorithms.

        Returns
        -------
        available_endpoints : list
            A list of the endpoints of the available privacy algorithms.
        """
        available_privacy_algorithms = list(cls)
        available_endpoints = [algorithm.route_name for algorithm in available_privacy_algorithms]
        return available_endpoints

    @classmethod
    def get_metadata(cls):
        """
        Return the metadata of the available privacy algorithms.

        Returns
        -------
        privacy_algorithm_metadata : list of dict
            The metadata of the available privacy algorithms.
            id : int
                The algorithm id.
            name: str
                The algorithm name.
            parameters: list of dict
                The parameters of the algorithm.
                unit : str
                    The unit of the parameter.
                unitSymbol : str
                    The unit symbol of the parameter.
                min : float
                    The minimum value of the parameter.
                max : float
                    The maximum value of the parameter.
                description : str
                    The description text of the parameter.
            description:
                The description text of the algorithm.
        """
        privacy_algorithms = list(cls)
        privacy_algorithm_metadata = []
        for algorithm in privacy_algorithms:
            privacy_algorithm_metadata.append({
                'id': algorithm.id,
                'name': algorithm.display_name,
                'parameters': algorithm.parameters,
                'description': algorithm.description
            })
        return privacy_algorithm_metadata

    def protect(self, route, parameters):
        """
        Protect the route with the privacy algorithm.

        Parameters
        ----------
        route : Route
            The route to protect.
        parameters : :obj:`list` of :obj:`int`
            The parameters of the privacy algorithm.

        Returns
        -------
        protected_route : Route
            The route protected with this privacy algorithm.
        """
        protected_route = Route()
        if self is PrivacyAlgorithm.NOISE2D_PER_POINT:
            [protected_route.append(noise2d.perturb(point, parameters[0])) for point in route]
        elif self is PrivacyAlgorithm.NOISE2D_PER_ROUTE:
            protected_route = noise2d_global.perturb(route, epsilon=parameters[0])
        elif self is PrivacyAlgorithm.PROMESSE:
            protected_route = promesse.speed_smoothing(route, parameters[0])
        return protected_route
