"""Provides an enum for all available metrics.
"""

from aenum import AutoNumberEnum

from de4l_privacy_services.services.metrics import dtour, stop_detection, traffic_density, euclidean_distance


class Metric(AutoNumberEnum):
    """Enum of all available metrics.
    """
    _init_ = 'route_name id display_name type description'
    STOP_DETECTION = \
        'stop_detection', 0, 'DJ Cluster', 'privacy', \
        'Detects Points of Interest (POIs) based on time- and space-based clustering and and compares them with the ' \
        'true stops. The privacy value refers to how close a detected POI is to a true POI. Implementation ' \
        'according to Primault, V. (2018).'
    DTOUR_GERMANY = \
        'dtour_germany', 1, 'Dtour Germany', 'privacy', \
        'Detects of points of interest (POIs) by calculating the optimal routes and determining the deviations of ' \
        'the actual routes from them as described in Schneider et al. (2022). The privacy value refers to the ' \
        '(average of the) inverse detection risk of a detected POI if it is within a 200 m distance to an actual POI.'
    DTOUR_PORTUGAL = \
        'dtour_portugal', 2, 'Dtour Portugal', 'privacy', \
        'Detects of points of interest (POIs) by calculating the optimal routes and determining the deviations of ' \
        'the actual routes from them as described in Schneider et al. (2022). The privacy value refers to the ' \
        '(average of the) inverse detection risk of a detected POI if it is within a 200 m distance to an actual POI.'
    DTOUR_CALIFORNIA = \
        'dtour_california', 3, 'Dtour California', 'privacy', \
        'Detects of points of interest (POIs) by calculating the optimal routes and determining the deviations of ' \
        'the actual routes from them as described in Schneider et al. (2022). The privacy value refers to the ' \
        '(average of the) inverse detection risk of a detected POI if it is within a 200 m distance to an actual POI.'
    RATIO_POINT_COUNT = \
        'ratio_of_point_count', 4, 'Ratio of point count', 'utility', \
        'Compares the protected data to the original data by creating a grid and counting the number of original and ' \
        'protected points in each cell. Since the original routes serve as a baseline, they have a utility of 100%.'
    RATIO_DESIRED_POINT_COUNT = \
        'ratio_of_desired_point_count', 5, 'Ratio of desired point count', 'utility', \
        'Compares the protected data to the original data by creating a grid and counting the number of original ' \
        'and protected points in each cell while also incorporating a desired point count of at least 10 points per ' \
        'cell of 1 square kilometers.'
    EUCLIDEAN_DISTANCE = \
        'euclidean_distance', 6, 'Euclidean distance', 'utility', \
        'Compares the protected data to the original data by calculating the euclidean distance from every point in ' \
        'the protected data to the closest point in its corresponding original route. The smaller the distance, the ' \
        'greater the utility. Since the original routes serve as a baseline, they have a utility of 100%.'

    @classmethod
    def get_available_endpoints(cls):
        """
        Returns the endpoints of the available metrics.

        Returns
        -------
        available_endpoints : list
            A list of the endpoints of the available metrics.
        """
        available_metrics = list(cls)
        available_endpoints = [metric.route_name for metric in available_metrics]
        return available_endpoints

    @classmethod
    def get_metadata(cls):
        """
        Return the metadata of the available metrics.

        Returns
        -------
        metric_metadata : list of dict
            The metadata of the available metrics.
            id : int
                The metric id.
            name: str
                The metric name.
            type: {'privacy', 'utility'}
                The type of the metric.
            description:
                The description text of the metric.
        """
        metrics = list(cls)
        metric_metadata = []
        for metric in metrics:
            metric_metadata.append({
                'id': metric.id,
                'name': metric.display_name,
                'type': metric.type,
                'description': metric.description
            })
        return metric_metadata

    def calculate(self, routes, true_pois=None, comparative_routes=None, parameters=None):
        """
        Calculate the metric value based on the route and optionally a route to compare with.

        Parameters
        ----------
        routes : :obj:`list` of :obj:`Route`
            The routes to calculate the metric for.
        true_pois : :obj:`list` of :obj:`Route`
            The true points of interest in each route. Used only with metrics 'DTOUR_*' and 'STOP_DETECTION'.
        comparative_routes : Route
            The routes to compare with. Used only when metrics compare the route data set with another.
        parameters : :obj:`list` of :obj:`float`
            The parameters of the metric.

        Returns
        -------
        metric_values : :obj:`list` of :obj:`list` of :obj:`int`
            The metric values for each point in each route, normalized to an interval [0, 100] where 0 is the lowest
            metric value (e.g. no privacy or utility) and 100 is the best value (e.g. perfect privacy or utility).
        performance_measures : dict
            Performance values (precision, recall, f-score) of the privacy metrics for the dataset and the routes. For
            the utility metrics it's empty.
        """
        metric_values = []
        performance_measures = []
        if self is Metric.DTOUR_GERMANY:
            metric_values, performance_measures = \
                dtour.calculate_dtour_metric(routes, true_pois, service_name='germany')
        elif self is Metric.DTOUR_PORTUGAL:
            metric_values, performance_measures = \
                dtour.calculate_dtour_metric(routes, true_pois, service_name='portugal')
        elif self is Metric.DTOUR_CALIFORNIA:
            metric_values, performance_measures = \
                dtour.calculate_dtour_metric(routes, true_pois, service_name='california')
        elif self is Metric.STOP_DETECTION:
            metric_values, performance_measures = stop_detection.calculate_stop_detection_metric(routes, true_pois)
        elif self is Metric.RATIO_POINT_COUNT:
            if parameters is None or len(parameters) == 0:
                metric_values = traffic_density.calculate_ratio_of_point_count(routes, comparative_routes)
            else:
                metric_values = traffic_density.calculate_ratio_of_point_count(routes, comparative_routes,
                                                                               width_per_cell=parameters[0])
        elif self is Metric.RATIO_DESIRED_POINT_COUNT:
            if parameters is None or len(parameters) < 4:
                metric_values = traffic_density.calculate_ratio_of_desired_point_count(routes, comparative_routes)
            else:
                metric_values = traffic_density.calculate_ratio_of_desired_point_count(
                    routes, comparative_routes, width_per_cell=parameters[2],
                    desired_point_count_per_cell=parameters[3])
        elif self is Metric.EUCLIDEAN_DISTANCE:
            if parameters is None or len(parameters) == 0:
                metric_values = euclidean_distance.calculate_euclidean_distance(routes, comparative_routes)
            else:
                metric_values = euclidean_distance.calculate_euclidean_distance(routes, comparative_routes,
                                                                                distance_limit=parameters[0])
        return metric_values, performance_measures
