"""Module to host privacy services with flask.
"""

import json
import copy

import numpy as np
from flask import Blueprint, Flask, Response, request
from de4l_geodata.geodata.route import Route
from de4l_geodata.geodata.point_t import PointT
from pandas import Timestamp

from de4l_privacy_services.services.available_privacy_algorithms import PrivacyAlgorithm
from de4l_privacy_services.services.available_metrics import Metric

bp = Blueprint('privacy_services', __name__)


def create_app(config='dev'):
    """
    Create a client to register routes.

    Parameters
    ----------
    config : {'dev', 'test'}
        The configuration setting. If 'test', the client will run in test mode.<

    Returns
    -------
    client : Flask
        A flask client.
    """
    client = Flask(__name__)
    client.config['TESTING'] = config == 'test'
    client.register_blueprint(bp)
    return client


@bp.route('/', methods=['GET'])
def welcome():
    """Shows a list of all available endpoints.
    """
    return list_available_endpoints()


@bp.route('/route', methods=['POST'])
def provide_metrics_and_optionally_protect_route():
    """
    Calculate different privacy and utility metrics for a set of routes. If requested, protect the routes with a
    privacy-preserving mechanism and calculate the metrics for the protected routes, too. Return the set of routes
    with the metrics, and, if requested, the protected routes with their metrics. Receive a POST request containing
    a dictionary with the following data:
        routes : :obj:`list` of :obj:`dict`
            A list of dictionaries with following data:
            id : int
                The route id.
            name : str
                The route name.
            date : Date
                The date of the route.
            points : :obj:`list` of :obj:`dict`
                A list of dictionaries of the route's points.
                id : int
                    The point id.
                timestamp : Date
                    The timestamp of the point.
                measurements : :obj:`list` of :obj:`dict`
                    type : string
                        The measurement type.
                    value : float
                        The measurement value.
                longitude : float
                    The longitude of the point in degrees.
                latitude : float
                    The latitude of the point in degrees.
            stops : :obj:`list` of :obj:`dict`
                A list of dictionaries of the route's stop points.
                pointId : int
                    The point id.
                timestamp : Date
                    The timestamp of the point.
                longitude : float
                    The longitude of the point in degrees.
                latitude : float
                    The latitude of the point in degrees.
        metricIds : :obj:`list` of :obj:`int`
            The indices of the metrics to calculate, referenced by the list of available metrics.
        calculateNonPrivateMetrics : bool
            If true, metrics for the non-protected routes are calculated and returned. (The calculation is independent
            of the setting, whether metrics for protected routes should be calculated.) If false, no metrics are
            calculated for the original non-protected routes.
        privacyAlgorithm : :obj:`dict`
            The privacy-preserving algorithm to protect the data with. If empty, protected routes and corresponding
            metrics will not be calculated.
            id : int
                The id of the privacy-preserving algorithm to protect the routes with, referenced by the list of
                available algorithms.
            parameters : :obj:`list` of :obj:`float`
                The parameters of the privacy-preserving algorithm.
        privateRoutes : :obj:`dict`
            A dictionary containing all information about the protected routes. If not empty, a privacy algorithm has
            already been applied but metrics need to be recalculated.
            routes : :obj:`list` of :obj:`dict`
                A list of dictionaries with following data:
                id : int
                    The route id.
                name : str
                    The route name.
                date : Date
                    The date of the route.
                metrics: :obj:`list` of :obj:`dict`
                    The metrics of the single protected route.
                    id : int
                        The metric id.
                    name: str
                        The metric name.
                    value: int
                        The metric value.
                    type: {'privacy', 'utility'}
                        The type of the metric.
                points : :obj:`list` of :obj:`dict`
                    A list of dictionaries of the route's points.
                    id : int
                        The point id.
                    timestamp : Date
                        The timestamp of the point.
                    longitude : float
                        The longitude of the point in degrees.
                    latitude : float
                        The latitude of the point in degrees.
                    metrics : :obj:`list` of :obj:`dict`
                        The metrics of the point.
                        id : int
                            The metric id.
                        name: str
                            The metric name.
                        value: int
                            The metric value.
                        type: {'privacy', 'utility'}
                            The type of the metric.
                    measurements : :obj:`list` of :obj:`dict`
                        type : string
                            The measurement type.
                        value : float
                            The measurement value.
                stops : :obj:`list` of :obj:`dict`
                    A list of dictionaries of the route's stop points.
                    pointId : int
                        The point id.
                    timestamp : Date
                        The timestamp of the point.
                    longitude : float
                        The longitude of the point in degrees.
                    latitude : float
                        The latitude of the point in degrees.
            metrics: :obj:`list` of :obj:`dict`
                The metrics of the set of protected routes.
                id : int
                    The metric id.
                name: str
                    The metric name.
                value: int
                    The metric value.
                type: {'privacy', 'utility'}
                    The type of the metric.

    Returns
    -------
    routes_and_metrics_response: Response
        A response object containing the set of routes with the metrics, and, if requested, the protected routes with
        their metrics. If an invalid endpoint is provided, an error message is returned. The response is a dictionary
        with the following keys:
        nonPrivateRoutes : :obj:`dict`
            A dictionary containing all information about the non-protected routes.
            routes : :obj:`list` of :obj:`dict`
                A list of dictionaries with following data:
                id : int
                    The route id.
                name : str
                    The route name.
                date : Date
                    The date of the route.
                metrics : :obj:`list` of :obj:`dict`
                    The metrics of the single non-protected route.
                    id : int
                        The metric id.
                    name : str
                        The metric name.
                    value : int
                        The metric value.
                    type : {'privacy', 'utility'}
                        The type of the metric.
                    performance : dict
                        Performance values for the route. Empty if type is 'utility'.
                        precision : float
                        recall : float
                        f-score : float
                points : :obj:`list` of :obj:`dict`
                    A list of dictionaries of the route's points.
                    id : int
                        The point id.
                    timestamp : Date
                        The timestamp of the point.
                    longitude : float
                        The longitude of the point in degrees.
                    latitude : float
                        The latitude of the point in degrees.
                    metrics : :obj:`list` of :obj:`dict`
                        The metrics of the point.
                        id : int
                            The metric id.
                        name: str
                            The metric name.
                        value: int
                            The metric value.
                        type: {'privacy', 'utility'}
                            The type of the metric.
                    measurements : :obj:`list` of :obj:`dict`
                        type : string
                            The measurement type.
                        value : float
                            The measurement value.
                stops : :obj:`list` of :obj:`dict`
                    A list of dictionaries of the route's stop points.
                    pointId : int
                        The point id.
                    timestamp : Date
                        The timestamp of the point.
                    longitude : float
                        The longitude of the point in degrees.
                    latitude : float
                        The latitude of the point in degrees.
            metrics: :obj:`list` of :obj:`dict`
                The metrics of the set of non-protected routes.
                id : int
                    The metric id.
                name: str
                    The metric name.
                value: int
                    The metric value.
                type: {'privacy', 'utility'}
                    The type of the metric.
                performance : dict
                    Performance values for the dataset. Empty if type is 'utility'.
                    precision : float
                    recall : float
                    f-score : float
        privateRoutes : :obj:`dict`
            A dictionary containing all information about the protected routes.
            routes : :obj:`list` of :obj:`dict`
                A list of dictionaries with following data:
                id : int
                    The route id.
                name : str
                    The route name.
                date : Date
                    The date of the route.
                metrics: :obj:`list` of :obj:`dict`
                    The metrics of the single protected route.
                    id : int
                        The metric id.
                    name: str
                        The metric name.
                    value: int
                        The metric value.
                    type: {'privacy', 'utility'}
                        The type of the metric.
                    performance : dict
                        Performance values for the route. Empty if type is 'utility'.
                        precision : float
                        recall : float
                        f-score : float
                points : :obj:`list` of :obj:`dict`
                    A list of dictionaries of the route's points.
                    id : int
                        The point id.
                    timestamp : Date
                        The timestamp of the point.
                    longitude : float
                        The longitude of the point in degrees.
                    latitude : float
                        The latitude of the point in degrees.
                    metrics : :obj:`list` of :obj:`dict`
                        The metrics of the point.
                        id : int
                            The metric id.
                        name: str
                            The metric name.
                        value: int
                            The metric value.
                        type: {'privacy', 'utility'}
                            The type of the metric.
                    measurements : :obj:`list` of :obj:`dict`
                        type : string
                            The measurement type.
                        value : float
                            The measurement value.
                stops : :obj:`list` of :obj:`dict`
                    A list of dictionaries of the route's stop points.
                    pointId : int
                        The point id.
                    timestamp : Date
                        The timestamp of the point.
                    longitude : float
                        The longitude of the point in degrees.
                    latitude : float
                        The latitude of the point in degrees.
            metrics: :obj:`list` of :obj:`dict`
                The metrics of the set of protected routes.
                id : int
                    The metric id.
                name: str
                    The metric name.
                value: int
                    The metric value.
                type: {'privacy', 'utility'}
                    The type of the metric.
                performance : dict
                    Performance values for the dataset. Empty if type is 'utility'.
                    precision : float
                    recall : float
                    f-score : float
    """
    dto_in = request.get_json()
    metrics = get_metrics(dto_in['metricIds'])
    point_ids = [point['id'] for route in dto_in['routes'] for point in route['points']]

    routes = []
    true_pois = []
    protected_routes = []
    metric_values = {'nonPrivateRoutes': [], 'privateRoutes': []}
    performance_measures = {'nonPrivateRoutes': [], 'privateRoutes': []}

    # extract routes and true pois, calculate optionally protected routes
    for route_dto in dto_in['routes']:
        route = points_dto_to_route(list(route_dto['points']))
        true_pois_route = points_dto_to_true_pois(route_dto['stops'])
        true_pois.append(true_pois_route)
        if dto_in['privacyAlgorithm'] is not None:
            protected_route = PrivacyAlgorithm(dto_in['privacyAlgorithm']['id'] + 1).\
                protect(route, parameters=dto_in['privacyAlgorithm']['parameters'])
            protected_route.to_degrees_()
            protected_routes.append(protected_route)
        route.to_degrees_()
        routes.append(route)

    if dto_in['privateRoutes'] is not None and dto_in['privacyAlgorithm'] is None:
        for route_dto in dto_in['privateRoutes']:
            route = points_dto_to_route(list(route_dto['points']))
            route.to_degrees_()
            protected_routes.append(route)

    # calculate metrics for the data set, the routes and the points
    for metric_id in dto_in['metricIds']:
        if len(protected_routes) > 0:
            metric_values_private, performance_measures_private = \
                Metric.calculate(Metric(metric_id + 1), protected_routes, true_pois, routes)
            metric_values['privateRoutes'].append(metric_values_private)
            performance_measures['privateRoutes'].append(performance_measures_private)

        if dto_in['calculateNonPrivateMetrics']:
            metric_values_non_private, performance_measures_non_private = \
                Metric.calculate(Metric(metric_id + 1), routes, true_pois, routes)
            metric_values['nonPrivateRoutes'].append(metric_values_non_private)
            performance_measures['nonPrivateRoutes'].append(performance_measures_non_private)

    # write information into dto
    dto_out = {'nonPrivateRoutes': None, 'privateRoutes': None}
    for calculation_name, is_to_calculate in \
            [('nonPrivateRoutes', dto_in['calculateNonPrivateMetrics']),
             ('privateRoutes', len(protected_routes) > 0)]:
        if is_to_calculate:
            dto_out[calculation_name] = {}
            dto_out[calculation_name]['routes'] = copy.deepcopy(dto_in['routes'])

            point_id = int(np.min(point_ids) if calculation_name == 'nonPrivateRoutes' else np.max(point_ids) + 1)
            for route_idx, route_dto in enumerate(dto_out[calculation_name]['routes']):
                if calculation_name == 'privateRoutes':
                    route = protected_routes[route_idx]
                    route_dto['stops'] = dto_in['routes'][route_idx]['stops']
                else:
                    route = routes[route_idx]
                # create metric dto for points and route and update route set dto
                metric_point_dto = []
                metric_route_dto = []
                for metric_idx, (metric_id, metric_name, metric_type) in enumerate(metrics):
                    metric_route_dto.append({
                        'id': metric_id, 'name': metric_name,
                        'value': metric_values[calculation_name][metric_idx]['route'][route_idx],
                        'type': metric_type,
                        'performance': performance_measures[calculation_name][metric_idx]['route'][route_idx]
                        if performance_measures[calculation_name][metric_idx] else None})
                    for point_idx, metric_value_point in \
                            enumerate(metric_values[calculation_name][metric_idx]['point'][route_idx]):
                        if len(metric_point_dto) <= point_idx:
                            metric_point_dto.append([])
                        metric_point_dto[point_idx].append({'id': metric_id, 'name': metric_name,
                                                            'value': metric_value_point, 'type': metric_type})

                # append points and metrics to the output dto
                route_dto['points'] = []
                for point_idx, point in enumerate(route):
                    measurements = []
                    if len(dto_in['routes'][route_idx]['points']) == len(route):
                        measurements = dto_in['routes'][route_idx]['points'][point_idx]['measurements']
                    point_dto = {'id': point_id,
                                 'timestamp': point.timestamp.isoformat(),
                                 'longitude': point.x_lon,
                                 'latitude': point.y_lat,
                                 'measurements': measurements,
                                 'metrics': metric_point_dto[point_idx] if point_idx < len(metric_point_dto) else None
                                 }
                    point_id += 1
                    route_dto['points'].append(point_dto)
                route_dto['metrics'] = metric_route_dto

            # write dataset metric into dto
            dto_out[calculation_name]['metrics'] = \
                [{
                    'id': metric_id, 'name': metric_name, 'type': metric_type,
                    'value': metric_values[calculation_name][metric_idx]['dataset'],
                    'performance': performance_measures[calculation_name][metric_idx]['dataset']
                    if performance_measures[calculation_name][metric_idx] else None
                  }
                 for metric_idx, (metric_id, metric_name, metric_type) in enumerate(metrics)]
    routes_and_metrics_response = Response(json.dumps(dto_out), mimetype='application/json')
    return routes_and_metrics_response


@bp.route('/metric', methods=['GET'])
def get_metric_metadata():
    """
    Return the metadata of all available metrics.

    Returns
    -------
    list of dict
        The metadata of the available metrics.
        id : int
            The metric id.
        name: str
            The metric name.
        type: {'privacy', 'utility'}
            The type of the metric.
        description:
            The description text of the metric.
    """
    return Metric.get_metadata()


@bp.route('/privacyAlgorithm', methods=['GET'])
def get_privacy_algorithm_metadata():
    """
    Return the metadata of all available privacy algorithms.

    Returns
    -------
    list of dict
        The metadata of the available privacy algorithms.
        id : int
            The algorithm id.
        name: str
            The algorithm name.
        parameters: list of dict
            The parameters of the algorithm.
            unit : str
                The unit of the parameter.
            unitSymbol : str
                The unit symbol of the parameter.
            min : float
                The minimum value of the parameter.
            max : float
                The maximum value of the parameter.
            description : str
                The description text of the parameter.
        description:
            The description text of the algorithm.
    """
    return PrivacyAlgorithm.get_metadata()


def list_available_endpoints():
    """
    Return a message listing all available endpoints.

    Returns
    -------
    message : str
        A message listing all available endpoints.
    """
    message = 'The following endpoints are available:\n' \
              '/route/ -> calculates metrics and optionally protects routes data'
    return message


def points_dto_to_route(points_dto):
    """
    Transform a route in points_dto format into a Route object with points in radians format. Assume that
    latitude/longitude pairs of each point are provided in degrees.

    Parameters
    ----------
    points_dto : :obj:`list` of :obj:`dict`
        The route's points.
        id : int
            The point id.
        timestamp : Date
            The timestamp of the point.
        longitude : float
            The longitude of the point in degrees.
        latitude : float
            The latitude of the point in degrees.
        measurements : :obj:`list` of :obj:`dict`
            type : string
                The measurement type.
            value : float
                The measurement value.

    Returns
    -------
    route : Route
        The point list transformed into a Route object where coordinates are in radians format.
    """
    route = Route()
    try:
        for point_dto in points_dto:
            point = PointT([point_dto['longitude'], point_dto['latitude']], Timestamp(point_dto['timestamp']),
                           coordinates_unit='degrees')
            route.append(point)
        route.to_radians_()
    except KeyError:
        print('Route could not be parsed:' + str(points_dto))
        raise
    return route


def points_dto_to_true_pois(stops):
    """
    Transform the list of stop points of a route into a Route object with points in radians format.
    Assume that latitude/longitude pairs of each point are provided in degrees.

    Parameters
    ----------
    stops : :obj:`list` of :obj:`dict`
        The route's stops.
        pointId : int
            The point id.
        timestamp : Date
            The timestamp of the point.
        longitude : float
            The longitude of the point in degrees.
        latitude : float
            The latitude of the point in degrees.

    Returns
    -------
    route : Route
        The stop points of the route transformed into a Route object where coordinates are in radians format.
    """
    route = Route()
    try:
        for stop_dto in stops:
            point = PointT([stop_dto['longitude'], stop_dto['latitude']], Timestamp(stop_dto['timestamp']),
                           coordinates_unit='degrees')
            route.append(point)
        route.to_radians_()
    except KeyError:
        print('Route could not be parsed:' + str(stops))
        raise
    return route


def get_metrics(metric_ids):
    """
    Parse information about the metrics defined by the metric ids.

    Parameters
    ----------
    metric_ids : :obj:`list` of :obj:`int`
        The indices of the metrics to calculate, referenced by the metric's id in the list of available metrics.

    Returns
    -------
    metrics : :obj:`list` of :obj:`tuple`
        Information about the metrics to calculate, including:
        int
            The metric id.
        str
            The name of the metric.
        {'privacy', 'utility'}
            The type of the metric.
    """
    available_metrics = list(Metric.get_available_endpoints())
    metrics = [(metric_id, available_metrics[metric_id], Metric(metric_id + 1).type) for metric_id in metric_ids
               if metric_id < len(available_metrics)]
    return metrics


if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0')
