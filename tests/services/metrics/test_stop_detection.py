"""Test of stop detection metric.
"""

import unittest

import pandas as pd
from de4l_geodata.geodata.route import Route
from de4l_geodata.geodata.point_t import PointT

from de4l_privacy_services.services.metrics import stop_detection


class TestTrafficDensity(unittest.TestCase):
    """Test of stop detection.
    """

    def setUp(self) -> None:
        self.routes = [
            Route([
                PointT([-8.639703, 41.159781], coordinates_unit='degrees', timestamp=pd.Timestamp(0)),
                PointT([-8.639757, 41.159754], coordinates_unit='degrees', timestamp=pd.Timestamp(1)),
                PointT([-8.639775, 41.159727], coordinates_unit='degrees', timestamp=pd.Timestamp(2)),
                PointT([-8.639901, 41.159655], coordinates_unit='degrees', timestamp=pd.Timestamp(3)),
                PointT([-8.640522, 41.15979], coordinates_unit='degrees', timestamp=pd.Timestamp(4)),
                PointT([-8.640513, 41.159808], coordinates_unit='degrees', timestamp=pd.Timestamp(5)),
                PointT([-8.640522, 41.159817], coordinates_unit='degrees', timestamp=pd.Timestamp(6))
            ]).to_radians()
        ]
        self.true_pois = [
            [self.routes[0][0]]
        ]
        self.empty_routes = [Route()]

    def test_stop_detection(self):
        metric_values, performance_measures = \
            stop_detection.calculate_stop_detection_metric(self.routes, self.true_pois)

        # metric values to not exceed the maximum value
        for route_value in metric_values['route']:
            self.assertLessEqual(route_value, 100)
        for route in metric_values['point']:
            for point_value in route:
                self.assertLessEqual(point_value, 100)
        self.assertLessEqual(metric_values['dataset'], 100)

        # for each point in a route there is a metric value available
        for route_idx, route in enumerate(self.routes):
            self.assertEqual(len(route), len(metric_values['point'][route_idx]))

        # if empty routes are supplied, the resulting metrics are at the maximum value
        metric_values, performance_measures = \
            stop_detection.calculate_stop_detection_metric(self.empty_routes, self.empty_routes)
        self.assertEqual([[]], metric_values['point'])
        for route_value in metric_values['route']:
            self.assertEqual(100, route_value)
        self.assertEqual(100, metric_values['dataset'])

