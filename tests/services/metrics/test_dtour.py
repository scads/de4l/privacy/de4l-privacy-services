"""Test of D-Tour metric.
"""

import unittest

from de4l_geodata.geodata.route import Route
from de4l_geodata.geodata.point import Point, get_distance

from de4l_privacy_services.services.metrics import dtour


class TestTrafficDensity(unittest.TestCase):
    """Test of traffic density.
    """

    def setUp(self) -> None:
        self.routes = [
            Route([[-8.639703, 41.159781], [-8.639757, 41.159754], [-8.639775, 41.159727], [-8.639901, 41.159655],
                   [-8.640522, 41.15979], [-8.640513, 41.159808], [-8.640522, 41.159817], [-8.640702, 41.159565],
                   [-8.641098, 41.15819700000001], [-8.641539, 41.156865], [-8.642151, 41.154939],
                   [-8.642988, 41.15421], [-8.645049000000002, 41.154381], [-8.646876, 41.154264],
                   [-8.649657, 41.154084000000005], [-8.651187, 41.152887], [-8.653356000000002, 41.152122],
                   [-8.656236, 41.151618], [-8.658414000000002, 41.150934], [-8.659152, 41.15223]],
                  coordinates_unit='degrees').to_radians()
        ]
        self.true_pois = [
            [Point(self.routes[0][0])]
        ]
        self.empty_routes = [Route()]

    def test_dtour(self):
        metric_values, performance_measures = \
            dtour.calculate_dtour_metric(self.routes, self.true_pois, service_name='portugal')

        # metric values to not exceed the maximum value
        for route_value in metric_values['route']:
            self.assertLessEqual(route_value, 100)
        for route in metric_values['point']:
            for point_value in route:
                self.assertLessEqual(point_value, 100)
        self.assertLessEqual(metric_values['dataset'], 100)

        # for each point in a route there is a metric value available
        for route_idx, route in enumerate(self.routes):
            self.assertEqual(len(route), len(metric_values['point'][route_idx]))

        # if empty routes are supplied, the resulting metrics are at the maximum value
        metric_values, performance_measures = \
            dtour.calculate_dtour_metric(self.empty_routes, self.empty_routes, service_name='portugal')
        self.assertEqual([[]], metric_values['point'])
        for route_value in metric_values['route']:
            self.assertEqual(100, route_value)
        self.assertEqual(100, metric_values['dataset'])
