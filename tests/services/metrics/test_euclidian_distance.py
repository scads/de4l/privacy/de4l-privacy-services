"""Test of traffic density metrics.
"""

import unittest

from de4l_geodata.geodata.route import Route
from de4l_geodata.geodata.point import Point

from de4l_privacy_services.services.metrics import euclidean_distance as ed


class TestTrafficDensity(unittest.TestCase):
    """Test of traffic density.
    """

    def setUp(self) -> None:
        point_1 = Point([0, 0.5], geo_reference_system='cartesian')
        point_2 = Point([1, 1], geo_reference_system='cartesian')
        point_3 = Point([0, 0], geo_reference_system='cartesian')
        point_4 = Point([3, 1], geo_reference_system='cartesian')
        self.original_routes = [Route([point_1, point_2])]
        self.comparative_routes = [Route([point_3, point_4])]

    def test_calculate_relative_grid_density_difference(self):
        utility_values = ed.calculate_euclidean_distance(self.original_routes, self.comparative_routes,
                                                         distance_limit=1)
        self.assertEqual([[50, 0.]], utility_values['point'])
        self.assertEqual([25], utility_values['route'])
        self.assertEqual(25, utility_values['dataset'])
