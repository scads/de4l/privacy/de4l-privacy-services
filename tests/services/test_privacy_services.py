"""Test of privacy service hosting.
"""

import unittest
from math import radians
import datetime as dt

from de4l_geodata.geodata.route import Route
from de4l_geodata.geodata.point_t import PointT
from pandas import Timestamp

import de4l_privacy_services.services.host_privacy_services as sv


class TestPrivacyServices(unittest.TestCase):
    """Test of privacy service hosting.
    """

    def setUp(self):
        self.app = sv.create_app(config='test')
        self.client = self.app.test_client()
        point1 = PointT([radians(12.3785), radians(51.3274)],
                        Timestamp(year=2021, month=12, day=3, hour=10, minute=2, second=55))
        point2 = PointT([radians(12.3721), radians(51.3274)],
                        Timestamp(year=2021, month=12, day=4, hour=15, minute=0, second=10))
        point3 = PointT([radians(12.3721), radians(51.3274)],
                        Timestamp(year=2021, month=12, day=4, hour=15, minute=0, second=10))
        self.route = Route([point1, point2, point3])
        point1_dto = {'longitude': 12.3785, 'latitude': 51.3274, 'timestamp': '2021-12-03T10:02:55',
                      'id': 1, 'routeId': 1, 'measurements': [{'value': 1, 'type': 'temperature'}]}
        point2_dto = {'longitude': 12.3721, 'latitude': 51.3274, 'timestamp': '2021-12-04T15:00:10',
                      'id': 2, 'routeId': 1, 'measurements': [{'value': None, 'type': None}]}
        point3_dto = {'longitude': 12.3721, 'latitude': 51.3274, 'timestamp': '2021-12-04T15:00:10',
                      'id': 3, 'routeId': 1, 'measurements': [{'value': None, 'type': None}]}
        self.point_dto = []
        self.point_dto.append(point1_dto)
        self.point_dto.append(point2_dto)
        self.point_dto.append(point3_dto)

    def test_list_available_endpoints(self):
        """Test listing of available endpoints when root page is called.
        """
        response = self.client.get("/")
        self.assertEqual(200, response.status_code)
        self.assertIn('The following endpoints are available:\n', response.data.decode())
        self.assertIn('/route/', response.data.decode())

    def test_points_dto_to_route(self):
        """Test point dto to route conversion.
        """
        self.assertEqual(self.route, sv.points_dto_to_route(self.point_dto))

    def test_provide_metrics_and_optionally_protect_route(self):
        """Test endpoint to protect routes and calculate metrics.
        """
        routes = [
            {'id': 1, 'name': 'route 1', 'date': dt.date.fromisocalendar(2022, 5, 1), 'points': self.point_dto,
             'stops': []},
            {'id': 2, 'name': 'route 2', 'date': dt.date.fromisocalendar(2022, 5, 2), 'points': self.point_dto,
             'stops': []}
        ]
        metrics = [3]   # STOP_DETECTION
        request_data = {'routes': routes, 'metricIds': metrics, 'calculateNonPrivateMetrics': True,
                        'privacyAlgorithm': {'id': 0, 'parameters': [0.2]}, 'privateRoutes': None}
        nr_routes = len(routes)
        nr_metrics = len(metrics)
        nr_points = len(routes[0]['points'])
        # test correct data format
        response = self.client.post('/route', json=request_data)
        dto_out = response.get_json()
        self.assertEqual(['nonPrivateRoutes', 'privateRoutes'], list(dto_out.keys()))
        for calculation in ['nonPrivateRoutes', 'privateRoutes']:
            # check metrics per route data set
            self.assertEqual(nr_metrics, len(dto_out[calculation]['metrics']))
            self.assertEqual(100, dto_out[calculation]['metrics'][0]['value'])
            # check nr routes
            self.assertEqual(nr_routes, len(dto_out[calculation]['routes']))
            # check metrics per route
            self.assertEqual(nr_metrics, len(dto_out[calculation]['routes'][0]['metrics']))
            self.assertEqual(100, dto_out[calculation]['routes'][0]['metrics'][0]['value'])
            # check nr points
            self.assertEqual(nr_points, len(dto_out[calculation]['routes'][0]['points']))
            # check metrics per point
            self.assertEqual(nr_metrics, len(dto_out[calculation]['routes'][0]['points'][0]['metrics']))
            self.assertEqual(100, dto_out[calculation]['routes'][0]['points'][0]['metrics'][0]['value'])
            # check keys of routes
            self.assertEqual(['date', 'id', 'name', 'points', 'stops', 'metrics'],
                             list(dto_out[calculation]['routes'][0].keys()))
        # check keys of route points
        self.assertEqual(['id', 'timestamp', 'longitude', 'latitude', 'measurements', 'metrics'],
                         list(dto_out['nonPrivateRoutes']['routes'][0]['points'][0].keys()))
        self.assertEqual(['id', 'timestamp', 'longitude', 'latitude', 'measurements', 'metrics'],
                         list(dto_out['privateRoutes']['routes'][0]['points'][0].keys()))

        # check point coordinates stay the same for non-private routes
        self.assertAlmostEqual(self.point_dto[0]['longitude'],
                               dto_out['nonPrivateRoutes']['routes'][0]['points'][0]['longitude'], places=3)
        self.assertAlmostEqual(self.point_dto[0]['latitude'],
                               dto_out['nonPrivateRoutes']['routes'][0]['points'][0]['latitude'], places=3)
        # check point coordinates have changed with privacy protection
        self.assertNotEqual(
            self.point_dto[0]['longitude'], dto_out['privateRoutes']['routes'][0]['points'][0]['longitude'])
        self.assertNotEqual(
            self.point_dto[0]['latitude'], dto_out['privateRoutes']['routes'][0]['points'][0]['latitude'])

    def test_get_metrics(self):
        """Test receiving information about metrics.
        """
        metric_ids = [0]
        self.assertEqual([(0, 'stop_detection', 'privacy')], sv.get_metrics(metric_ids))


if __name__ == '__main__':
    unittest.main()
