FROM python:3.10.10-slim-bullseye as updated
RUN apt-get update -y && apt-get upgrade -y 

FROM updated as requirements
RUN apt-get install curl -y
RUN curl -sSL https://install.python-poetry.org | python3 -
WORKDIR /app
COPY . .
RUN $HOME/.local/bin/poetry config virtualenvs.create false 
RUN $HOME/.local/bin/poetry install --only main
RUN $HOME/.local/bin/poetry export --without-hashes >> requirements.txt

FROM updated as run 
RUN pip install torch
RUN apt-get install git -y
RUN pip install gunicorn
WORKDIR /app
COPY . .
COPY --from=requirements /app/requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
CMD gunicorn -w 4 -b 0.0.0.0:5000 --pythonpath './de4l_privacy_services/services' 'host_privacy_services:create_app()'
